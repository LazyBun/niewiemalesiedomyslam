package piszkod.pl.niewiemalesiedomyslam;

import android.database.MatrixCursor;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PlayersList extends AppCompatActivity {

    PlayerListAdapter adapter;

    public static final int playersAmount = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_players_list);

        final MediaPlayer trueSound = MediaPlayer.create(this, R.raw.true_sound);

        final Button buttonTrue = (Button) findViewById(R.id.buttonTrue);
        buttonTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trueSound.start();
            }
        });

        List<PlayerRow> list = new ArrayList<>();
        for(int i=1; i<=playersAmount; i++) {
            PlayerRow playerRow = new PlayerRow();
            playerRow.name = String.valueOf(i);
            playerRow.lifes = 3;
            playerRow.points = 0;
            list.add(playerRow);
        }
        adapter = new PlayerListAdapter(this);
        adapter.updateList(list);
        final ListView playersList = (ListView) findViewById(R.id.playersList);
        playersList.setAdapter(adapter);

    }


}
