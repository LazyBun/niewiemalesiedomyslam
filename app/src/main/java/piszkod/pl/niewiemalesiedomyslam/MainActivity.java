package piszkod.pl.niewiemalesiedomyslam;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MediaPlayer startSound = MediaPlayer.create(this, R.raw.start_sound);

        ImageButton startGameButton = (ImageButton) findViewById(R.id.imageButton);
        if (startGameButton != null) {
            startGameButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), PlayersList.class);
                    startActivity(intent);
                    startSound.start();
                }
            });
        }

    }
}
