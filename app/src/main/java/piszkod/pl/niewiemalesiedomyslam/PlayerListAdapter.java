package piszkod.pl.niewiemalesiedomyslam;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;

/**
 * Created by BułaLeniwa on 21.05.2016.
 */
public class PlayerListAdapter extends BaseAdapter {

    private List<PlayerRow> list = Collections.EMPTY_LIST;
    private final Context context;
    private final MediaPlayer falseSound;
    private final MediaPlayer trueSound;

    public static final int finalsPlayers = 3;

    public PlayerListAdapter(Context context) {
        this.context = context;
        falseSound = MediaPlayer.create(context, R.raw.false_sound);
        trueSound = MediaPlayer.create(context, R.raw.true_sound);
    }

    public void updateList(List<PlayerRow> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public PlayerRow getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.players_list_line, parent, false);
        }
        PlayerRow playerRow = getItem(position);
        TextView playerName = (TextView) convertView.findViewById(R.id.txtPlayerName);
        RatingBar playerLifes = (RatingBar) convertView.findViewById(R.id.ratingPlayerLifes);
        TextView playerPoints = (TextView) convertView.findViewById(R.id.txtPlayerPoints);
        Button btnAddPoints = (Button) convertView.findViewById(R.id.btnAddPoints);
        Button btnRemovePoints = (Button) convertView.findViewById(R.id.btnSubPoints);
        Button btnResetLifes = (Button) convertView.findViewById(R.id.btnResetLifes);

        playerName.setText(String.format("G: %s", playerRow.name));
        playerLifes.setRating(playerRow.lifes);
        playerPoints.setText(String.format("P: %s", String.valueOf(playerRow.points)));

        playerLifes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    PlayerRow playerRow = getItem(position);
                    if(playerRow.lifes > 0) {
                        playerRow.lifes -= 1;
                        falseSound.start();
                        if(playerRow.lifes == 0 && list.size() > 3) {
                            list.remove(position);
                            if(list.size() == finalsPlayers) {
                                //finals
                                for (PlayerRow row : list) {
                                    row.points = (int) row.lifes;
                                    row.lifes = 3;
                                }
                            }
                        }
                    }
                    notifyDataSetChanged();
                }
                return true;
            }
        });

        btnAddPoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerRow playerRow = getItem(position);
                playerRow.points += 10;
                trueSound.start();
                notifyDataSetChanged();
            }
        });
        btnRemovePoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerRow playerRow = getItem(position);
                playerRow.points -= 10;
                notifyDataSetChanged();
            }
        });
        btnResetLifes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerRow playerRow = getItem(position);
                playerRow.lifes += (playerRow.lifes < 3 ? 1 : 0);
                notifyDataSetChanged();
            }
        });

        if(list.size() >finalsPlayers) {
            btnAddPoints.setVisibility(View.GONE);
            btnRemovePoints.setVisibility(View.GONE);
            playerPoints.setVisibility(View.GONE);
        } else {
            btnAddPoints.setVisibility(View.VISIBLE);
            btnRemovePoints.setVisibility(View.VISIBLE);
            playerPoints.setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}
